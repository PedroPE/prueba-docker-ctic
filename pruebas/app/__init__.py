from flask import Flask, render_template, request, url_for
from flask_pymongo import PyMongo
from flask import jsonify
from wtforms import Form

app = Flask(__name__)

app.config["MONGO_URI"] = "mongodb://mongo:27017/prueba"
mongo=PyMongo(app)
@app.route("/index", methods=['GET', 'POST'])
def index():
	if(request.method=='POST'):
		if('submit' in request.form):
			nombre= request.form['Nombre']
			latitud= request.form['Latitud']
			longitud= request.form['Longitud']
			data={
			"Nombre": nombre,
			"Latitud":latitud,
			"Longitud":longitud
			}
			print(nombre=="")
			if(not(nombre=="")):
				mongo.db.personas.insert_one(data)
		else:
			mongo.db.personas.drop()

	lista_personas= mongo.db.personas.find()
	return render_template('index.html', lista_personas = lista_personas)

if __name__ == '__main__':
    app.run(debug=True,port=5000,host=('0.0.0.0'))
